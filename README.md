# OVERVIEW #

Hello World. This place will contain notes and agreements on how we work with code.

This is open source, and anyone can contribute and suggest ideas. Please let great ideas flow.

# DEFINITION OF TERMS #

**Trunk**

Every application will have an official main repo. This main repo is what we call 'trunk'.

**master**

We will follow a single branch development flow. We will not use a different branch for development or any other versions, unless it makes business sense. All changes must go to the 'master' branch, eventually.

# CONTRIBUTING CODE #

To contribute code, please take note of the following:

* Please do a private fork, for your own use.
* Clone your own forked repo, and develop from there.
* For new changes, create a new branch, and store your changes there.
* When you are ready to share, push it to your private fork, and create a PR to the trunk repo.

# CODE REVIEW #

We encourange to **pair with another programmer**, while you code. Specially for critical pieces.

Though at a minimum, we ask that at least **1** other programmer has reviewed your PR, before it is approved and merged.

# PROGRAMMER'S CODE #

In order to defend and preserve the honor and profession of computer programmers.

I promise that:

1. I will not produce a harmful code.
2. The code that I produce will always be my best work.
3. I will provide with each release a quick sure and repeatable proof that the code works as it's suppose to.
4. I will make frequent small release, I will not impede progress.
5. I will fearlessly and relentlessly improve the code in every opportunity. I will never make the code worst.
6. I will keep productivity my own and my team high. I will do nothing that decreases that productivity.
7. I will continuously ensure that others can cover for me and that I can cover for them.
8. I will produce estimates that are honest in both magnitude and precision. I will not make promises without certainty.
9. I will never stop learning and improving my craft.

Originally from Uncle Bob:
[http://blog.cleancoder.com/uncle-bob/2015/11/18/TheProgrammersOath.html](http://blog.cleancoder.com/uncle-bob/2015/11/18/TheProgrammersOath.html)
